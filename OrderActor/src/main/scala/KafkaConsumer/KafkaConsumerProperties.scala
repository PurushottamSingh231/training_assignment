package KafkaConsumer

import java.util.Properties

import akka.actor.Actor
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.slf4j.{Logger, LoggerFactory}

/** KafkaConsumer.KafkaConsumerUtil is the utility class to handle consumer property and consumer action */
class KafkaConsumerProperties {
  val logger: Logger = LoggerFactory.getLogger(this.getClass)
  val conf: Config = ConfigFactory.load()

  /** createConsumerProperties accept one argument groupId and return the properties for the consumer
   *
   * @param groupId Group ID for the consumer Group
   * @return configuration for the consumer
   */
  def createConsumerProperties(groupId: String): Properties = {

    val properties = new Properties()
    properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, conf.getString("kafkaConfig.bootstrap_server"))
    properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
    properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, conf.getString("kafkaConfig.auto_commit"))
    properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId)
    properties
  }
}

//  /** This method start SusOrderConsumer and read data from kafka and invoke CreateDoc method
//   *
//   * @param props configuration of teh consumer
//   * @param topic topic name from which the consumer will read data
//   */
//  def createSusOrderConsumer(props : Properties, topic: String) ={
//    val kafkaConsumer = new KafkaConsumer[String,String](props)
//    kafkaConsumer.subscribe(util.Arrays.asList(topic))  //subscribing list of topic
//    try {
//    while (true){
//      val consumerRecords: ConsumerRecords[String, String] = kafkaConsumer.poll(Duration.ofMillis(100)) //new version of poll
//      for(record <- consumerRecords.asScala){
//        logger.info("Key : " + record.key() + "\nValue : " + record.value())
//        logger.info("partition : " + record.partition() + "\nOffset : " + record.offset())
////        POC.createDoc(record.value().toString)
//      }
//    }
//    }catch{
//      case e : WakeupException => logger.info("consumer.poll interrupted ", e)
//    }finally {
////      kafkaConsumer.close(Duration.ofSeconds(1000))
//    }
//  }
//
//  /** This method start SusOrderConsumer and read data from kafka and invoke UpdateDriverDetails method
//   *
//   * @param props configuration of teh consumer
//   * @param topic topic name from which the consumer will read data
//   */
//  def createMobileStatusConsumer(props : Properties, topic: String) ={
//    val kafkaConsumer = new KafkaConsumer[String,String](props)
//    kafkaConsumer.subscribe(util.Arrays.asList(topic))  //subscribing list of topic
//    try{
//    while (true){
//      val consumerRecords: ConsumerRecords[String, String] = kafkaConsumer.poll(Duration.ofMillis(100)) //new version of poll
//      for(record <- consumerRecords.asScala){
//        logger.info("Key : " + record.key() + "\nValue : " + record.value())
//        logger.info("partition : " + record.partition() + "\nOffset : " + record.offset())
////        POC.updateDoc(record.value().toString)
//      }
//    }
//    }catch{
//      case e : WakeupException => logger.info("consumer.poll interrupted ", e)
//    }finally {
////      kafkaConsumer.close(Duration.ofSeconds(1000))
//    }
//  }
//
//  /** This method start SusOrderConsumer and read data from kafka and invoke update_driver_and _unset_cash_details method
//   *
//   * @param props configuration of teh consumer
//   * @param topic topic name from which the consumer will read data
//   */
//  def createSusDriverDetailConsumer(props : Properties, topic: String) ={
//    val kafkaConsumer = new KafkaConsumer[String,String](props)
//    kafkaConsumer.subscribe(util.Arrays.asList(topic))  //subscribing list of topic
//    try{
//    while (true){
//      val consumerRecords: ConsumerRecords[String, String] = kafkaConsumer.poll(Duration.ofMillis(100)) //new version of poll
//      for(record <- consumerRecords.asScala){
//        logger.info("Key : " + record.key() + "\nValue : " + record.value())
//        logger.info("partition : " + record.partition() + "\nOffset : " + record.offset())
////        POC.updateCash(record.value().toString)
//      }
//    }
//    }catch{
//      case e : WakeupException => logger.info("consumer.poll interrupted ", e)
//    }finally {
////      kafkaConsumer.close(Duration.ofSeconds(1000))
//    }
//  }
//}
