package ActorModel.OrderEventHandler

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, OneForOneStrategy, Props, SupervisorStrategy}
import akka.routing._
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import scala.concurrent.duration._
import scala.language.postfixOps

case class SusDelDriverDetail(delDriverDetail : String)
class SusDelDriverEventHandler extends Actor{
  var msg: Int = 0
  override def preStart(): Unit = println(s"MobileOrderStatusEventHandler starting...")
  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    super.preRestart(reason,message)  // stops all children, calls postStop( ) for crashing actor
    println(s"MobileOrderStatusEventHandler restarting...")
  }
  override def postRestart(reason: Throwable): Unit = println(s"MobileOrderStatusEventHandler Post restarted...")

  override def receive: Receive = orderEventHandling

  def orderEventHandling : Receive = {
    case orderData: SusDelDriverDetail => {println("Test passed for SusDelDriverData with input "+ orderData.delDriverDetail)}
    case orderData: SusDelDriverDetail => {
      msg += 1
      val jsonParser = new JSONParser
      val jsonObject = jsonParser.parse(orderData.delDriverDetail).asInstanceOf[JSONObject]
      val driverstatus = jsonObject.get("driver_Status").asInstanceOf[JSONObject]
      val driver_event = driverstatus.get("event_name").asInstanceOf[String]
      println(s"This is $msg message from topic 1 :==== $driver_event")
      driver_event match {
        case "Dispatch" => println("Hey we received new Order with event 0")
        case "Undispatch" => println("Hey we received change Order with event 1")
        case _ => printf("log info")  //info log
      }
    }
    case _ =>{
      println("Unhandled message")  //Error log
    }
  }
  override def postStop(): Unit = println(s"actor stopping...")

}

object SusDelDriverEventHandler{
  def props = Props(classOf[SusDelDriverEventHandler])

  def propsWithDispatcherAndRoundRobinRouter(dispatcher: String, nrOfInstances: Int): Props = {
    props.withDispatcher(dispatcher).withRouter(RoundRobinPool(nrOfInstances = nrOfInstances, supervisorStrategy = one))
  }
  def one: SupervisorStrategy = {
    OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }
  }
}
