package ActorModel.OrderEventHandler

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, OneForOneStrategy, Props, SupervisorStrategy}
import akka.routing._
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import scala.concurrent.duration._
import scala.language.postfixOps

case class MobileOrderStatus(mobileOrderTicket : String)

class MobileOrderStatusEventHandler extends Actor{

  var msg: Int = 0
  override def preStart(): Unit = println(s"MobileOrderStatusEventHandler starting...")
  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    super.preRestart(reason,message)  // stops all children, calls postStop( ) for crashing actor
    println(s"MobileOrderStatusEventHandler restarting...")
  }
  override def postRestart(reason: Throwable): Unit = println(s"MobileOrderStatusEventHandler Post restarted...")

  override def receive: Receive = orderEventHandling

  def orderEventHandling : Receive = {
    case orderData: MobileOrderStatus => {println("Test Passed for mobile order status with input"+orderData.mobileOrderTicket)}
    case orderData: MobileOrderStatus => {
      msg += 1
      val jsonParser = new JSONParser
      val jsonObject = jsonParser.parse(orderData.mobileOrderTicket).asInstanceOf[JSONObject]
      val msgType = jsonObject.get("messageType").asInstanceOf[String]
      val msgSubType  = jsonObject.get("messageSubType").asInstanceOf[String]


      println(s"This is $msg message from topic 1 :==== $msgType")

      (msgType,msgSubType) match {
        case ("orderAcceptance", "accepted") => println("Hey we received new Order with event 0")
        case (_, "delivered") => println("Hey we received change Order with event 1")
        case (_, "undelivered") => println("Hey we received change Pay with event 2")
        case _ => println(s"Unhandled msg $orderData") // info Log
      }
    }
    case _ =>{
      println("Unhandled message") //Error log
    }
  }
  override def postStop(): Unit = println(s"actor stopping...")
}

object MobileOrderStatusEventHandler {
  def props = Props(classOf[MobileOrderStatusEventHandler])

  def propsWithDispatcherAndRoundRobinRouter(dispatcher: String, nrOfInstances: Int): Props = {
    props.withDispatcher(dispatcher).withRouter(RoundRobinPool(nrOfInstances = nrOfInstances, supervisorStrategy = one))
  }

  def one: SupervisorStrategy = {
    OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }
  }
}
