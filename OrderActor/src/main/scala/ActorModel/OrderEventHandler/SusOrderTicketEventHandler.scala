package ActorModel.OrderEventHandler

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, OneForOneStrategy, Props, SupervisorStrategy}
import akka.routing._
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import scala.concurrent.duration._
import scala.language.postfixOps

case class susOrderTicket(orderDetail : String)

class SusOrderTicketEventHandler extends Actor {
  var msg: Int = 0
  override def preStart(): Unit = println(s"orderEventHandler starting...")
  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    super.preRestart(reason,message)  // stops all children, calls postStop( ) for crashing actor
    println(s"orderEventHandler restarting...")
  }
  override def postRestart(reason: Throwable): Unit = println(s"orderEventHandler Post restarted...")

  override def receive: Receive = orderEventHandling

  def orderEventHandling : Receive = {
    case orderdata : susOrderTicket => {println("Test Passed for orderData with input " + orderdata.orderDetail)}
    case orderData: susOrderTicket => {
      msg += 1
      val jsonParser = new JSONParser
      val jsonObject = jsonParser.parse(orderData.orderDetail).asInstanceOf[JSONObject]
      val orderEvent = jsonObject.get("header").asInstanceOf[JSONObject].get("event_name").asInstanceOf[String]

      println(s"This is $msg message from topic 1 :==== $orderEvent")
      orderEvent match {
        case "New order" => println("Hey we received new Order with event 0") //Insert Document
        case "Change order" => println("Hey we received change Order with event 1") //Upsert Document
        case "Change pay" => println("Hey we received change Pay with event 2")  // Upsert Document
        case "Order cashout" => println("Hey we received order cash Out with event 3") //log info
        case "Order cancel" => println("Hey we received order cancel with event 4")  //update with Driverdetails
        case "Order uncancel" => println("Hey we received orderUncancel with event 5") //upsert
        case "Order undeliverable" => println("Hey we received OrderUndelivered with event 6") //update Undelivered flag
        case _ => println("info log") //log info
      }
    }
    case _ =>{
      println("Unhandled message")  //log Error
    }
  }
  override def postStop(): Unit = println(s"actor stopping...")
}

object SusOrderTicketEventHandler {
  def props = Props(classOf[SusOrderTicketEventHandler])

  def propsWithDispatcherAndRoundRobinRouter(dispatcher: String, nrOfInstances: Int): Props = {
    props.withDispatcher(dispatcher).withRouter(RoundRobinPool(nrOfInstances = nrOfInstances, supervisorStrategy = one))
  }

  def one: SupervisorStrategy = {
    OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }
  }
}
