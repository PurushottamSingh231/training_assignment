package ActorModel.ConsumerActor

import java.time.Duration
import java.util
import ActorModel.OrderEventHandler.{SusOrderTicketEventHandler, susOrderTicket}
import KafkaConsumer.KafkaConsumerProperties
import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.errors.WakeupException
import org.slf4j.{Logger, LoggerFactory}
import scala.jdk.CollectionConverters._

case object readOrderTicket

class ReadSusOrderTicket extends Actor{

  val logger: Logger = LoggerFactory.getLogger(this.getClass)
  val conf: Config = ConfigFactory.load()
  val orderEventHandler : ActorRef = context.actorOf(SusOrderTicketEventHandler.propsWithDispatcherAndRoundRobinRouter(
    "default-dispatcher", 3), name="orderEventHandler")

  override def receive: Receive = {
    case orderDetail : susOrderTicket => { orderEventHandler ! orderDetail }
    case readOrderTicket =>{
      val kafkaCons = new KafkaConsumerProperties
      val prop = kafkaCons.createConsumerProperties(conf.getString("kafkaConfig.susOrderTicketGroupIdName"))
      val susOrderTicketConsumer = new KafkaConsumer[String,String](prop)
      susOrderTicketConsumer.subscribe(util.Arrays.asList(conf.getString("kafkaConfig.susOrderTicketTopicName")))
      while(true){
        try{
          val orderRecords: ConsumerRecords[String, String] = susOrderTicketConsumer.poll(Duration.ofMillis(conf.getInt(
            "kafkaConfig.kafkaTimeout")))
          for(record <- orderRecords.asScala){
            logger.info("Key : " + record.key() + "\nValue : " + record.value())
            logger.info("partition : " + record.partition() + "\nOffset : " + record.offset())
            orderEventHandler ! susOrderTicket(record.value())
          }
      }catch{
        case e : WakeupException => logger.info("consumer.poll interrupted ", e)
      }
      }
    }
    case _ => unhandled()
  }

}

object ReadSusOrderTicket {
  def props = Props(classOf[ReadSusOrderTicket])

}