package ActorModel.ConsumerActor

import java.time.Duration
import java.util
import ActorModel.OrderEventHandler.{MobileOrderStatus, MobileOrderStatusEventHandler}
import _root_.KafkaConsumer.KafkaConsumerProperties
import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.errors.WakeupException
import org.slf4j.{Logger, LoggerFactory}
import scala.jdk.CollectionConverters._

case object readMobileOrderStatusTicket
class ReadMobileOrderStatusDetails extends Actor{

  val logger: Logger = LoggerFactory.getLogger(this.getClass)
  val conf: Config = ConfigFactory.load()
  val mobileOrderEventHandler : ActorRef = context.actorOf(MobileOrderStatusEventHandler.propsWithDispatcherAndRoundRobinRouter(
    "default-dispatcher", 3), name="MobileOrderStatusEventHandler")

  override def receive: Receive = {
    case orderDetail : MobileOrderStatus => { mobileOrderEventHandler ! orderDetail }
    case readMobileOrderStatusTicket => {
      val kafkaCons = new KafkaConsumerProperties
      val prop = kafkaCons.createConsumerProperties("kafkaConfig.mobileOrderStatusTicketGroupIdName")
      val susOrderTicketConsumer = new KafkaConsumer[String,String](prop)
      susOrderTicketConsumer.subscribe(util.Arrays.asList(conf.getString("kafkaConfig.mobileOrderStatusTicketTopicName")))
      while(true){
        try{
          val orderRecords: ConsumerRecords[String, String] = susOrderTicketConsumer.poll(Duration.ofMillis(conf.getInt(
            "kafkaConfig.kafkaTimeout")))
          for(record <- orderRecords.asScala){
            logger.info("Key : " + record.key() + "\nValue : " + record.value())
            logger.info("partition : " + record.partition() + "\nOffset : " + record.offset())
            mobileOrderEventHandler ! MobileOrderStatus(record.value())
          }
        }catch{
          case e : WakeupException => logger.info("consumer.poll interrupted ", e)
        }
      }
    }
    case _ => unhandled()
  }

}

object ReadMobileOrderStatusDetails{
  def props = Props(classOf[ReadMobileOrderStatusDetails])
}