package ActorModel.ConsumerActor

import java.time.Duration
import java.util
import ActorModel.OrderEventHandler.{SusDelDriverDetail, SusDelDriverEventHandler}
import _root_.KafkaConsumer.KafkaConsumerProperties
import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.errors.WakeupException
import org.slf4j.{Logger, LoggerFactory}
import scala.jdk.CollectionConverters._

case object readSusDelDriverOrderTicket
class ReadSusDelDriverDetail extends Actor {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)
  val conf: Config = ConfigFactory.load()
  val orderEventHandler : ActorRef = context.actorOf(SusDelDriverEventHandler.propsWithDispatcherAndRoundRobinRouter(
    "default-dispatcher", 3), name="susDelDriverEventHandler")

  override def receive: Receive = {
    case orderDetail : SusDelDriverDetail => { orderEventHandler ! orderDetail }
    case readSusDelDriverOrderTicket =>{
      val kafkaCons = new KafkaConsumerProperties
      val prop = kafkaCons.createConsumerProperties(conf.getString("kafkaConfig.susDelDriverTicketGroupIdName"))
      val susOrderTicketConsumer = new KafkaConsumer[String,String](prop)
      susOrderTicketConsumer.subscribe(util.Arrays.asList(conf.getString("kafkaConfig.susDelDriverTicketTopicName")))
      while(true){
        try{
          val orderRecords: ConsumerRecords[String, String] = susOrderTicketConsumer.poll(Duration.ofMillis(conf.getInt(
            "kafkaConfig.kafkaTimeout")))
          for(record <- orderRecords.asScala){
            logger.info("Key : " + record.key() + "\nValue : " + record.value())
            logger.info("partition : " + record.partition() + "\nOffset : " + record.offset())
            orderEventHandler ! SusDelDriverDetail(record.value())
          }
        }catch{
          case e : WakeupException => logger.info("consumer.poll interrupted ", e)
        }
      }
    }
    case _ => unhandled()
  }
}

object ReadSusDelDriverDetail {
  def props = Props(classOf[ReadSusDelDriverDetail])

}
