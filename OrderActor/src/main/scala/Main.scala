import ActorModel.ConsumerActor.ReadSusDelDriverDetail
import ActorModel.OrderEventHandler.SusDelDriverDetail
import akka.actor.{Actor, ActorSystem, PoisonPill}
import com.typesafe.config.{Config, ConfigFactory}

object Main {
  val conf: Config = ConfigFactory.load()
  def main(args: Array[String]): Unit = {
      val _system: ActorSystem = ActorSystem.create("sus-manager-actor-system", conf.getConfig("configuration"))
//      val susOrderTicketReader = _system.actorOf(ReadSusOrderTicket.props, "orderTicketReader")
//      val mobileOrderStatusReader = _system.actorOf(ReadMobileOrderStatusDetails.props, "readMobileStatusTicketReader")
//      val susDelDriverReader = _system.actorOf(ReadSusDelDriverDetail.props, "ReadSusDelDriverTicketReader")
//      susOrderTicketReader ! readOrderTicket
//      mobileOrderStatusReader ! readMobileOrderStatusTicket
//      susDelDriverReader ! readSusDelDriverOrderTicket


    val orderReader = _system.actorOf(ReadSusDelDriverDetail.props, "ReadSusDelDriverTicketReader")
    orderReader ! SusDelDriverDetail("""{"event": 0,"ticketNum": 101,"ticketGuid": "780001SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
    orderReader ! SusDelDriverDetail("""{"event": 1,"ticketNum": 101,"ticketGuid": "780002SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")

    orderReader ! SusDelDriverDetail("""{"event": 2,"ticketNum": 101,"ticketGuid": "780003SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")

    orderReader ! SusDelDriverDetail("""{"event": 3,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")

    orderReader ! SusDelDriverDetail("""{"event": 4,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
    orderReader ! SusDelDriverDetail("""{"event": 5,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
    orderReader ! SusDelDriverDetail("""{"event": 6,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
    orderReader ! SusDelDriverDetail("""{"event": 1,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
    orderReader ! SusDelDriverDetail("""{"event": 2,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
    orderReader ! SusDelDriverDetail("""{"event": 3,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")



      //    orderReader ! susOrderTicket("""{"event": 2,"ticketNum": 101,"ticketGuid": "780003SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
//    orderReader ! susOrderTicket("""{"event": 3,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
//
//      orderReader ! susOrderTicket("""{"event": 4,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
//      orderReader ! susOrderTicket("""{"event": 5,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
//      orderReader ! susOrderTicket("""{"event": 6,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
//      orderReader ! susOrderTicket("""{"event": 1,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
//      orderReader ! susOrderTicket("""{"event": 2,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")
//      orderReader ! susOrderTicket("""{"event": 3,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime": "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,"tipAmount": 100}]}""")

    Thread.sleep(2 * 1000)
    orderReader ! PoisonPill
    _system.terminate()

  }

}
