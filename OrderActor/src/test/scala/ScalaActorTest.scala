import ActorModel.OrderEventHandler.{MobileOrderStatus, MobileOrderStatusEventHandler, SusDelDriverDetail, SusDelDriverEventHandler, SusOrderTicketEventHandler, susOrderTicket}
import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, MustMatchers}

class ScalaActorTest extends TestKit(ActorSystem("sus-order-ticket-test"))
    with ImplicitSender
    with FlatSpecLike
    with MustMatchers
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A ReadSusOrderTicket " should
    "send the received msg to their child SusOrderEventHandler" in {

      val orderReader = TestProbe()
      val susOrderReader = system.actorOf(Props[SusOrderTicketEventHandler])
      orderReader.send(susOrderReader, susOrderTicket(
        """{"event": 0,"ticketNum": 101,"ticketGuid": "780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime":
          | "111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,
          | "tipAmount": 100}]}"""))
      orderReader.send(susOrderReader, "abcd")
      expectNoMessage()
    }

  "A ReadMobileOrderStatusDetails " should
    "send the received msg to their child MobileOrderStatusEventHandler" in {

    val mobileOrderStatusReader = TestProbe()
    val mobilestatusEventHandler = system.actorOf(Props[MobileOrderStatusEventHandler])
    mobileOrderStatusReader.send(mobilestatusEventHandler, MobileOrderStatus(
      """{"event": 0,"ticketNum": 101,"ticketGuid":"780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime":
        |"111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum": 15,
        |"tipAmount": 100}]}"""))
    mobileOrderStatusReader.send(mobilestatusEventHandler, "abcd")
    expectNoMessage()
  }

  "A ReadSusDelDriverDetails " should
    "send the received msg to their child SusDelDriverEventHandler" in {

    val susDelDriverStatusReader = TestProbe()
    val susDelDriverEventHandler = system.actorOf(Props[SusDelDriverEventHandler])
    susDelDriverStatusReader.send(susDelDriverEventHandler, SusDelDriverDetail(
      """{"event": 0,"ticketNum": 101,"ticketGuid":"780004SOUR879NDJDUENKEN48","undeliveredFlag": "N","deliveryTime":
        |"111534","delayedPaymentTip": 0,"cardPaymentDetails": [{"cardInvoiceNum": 23,"tipAmount": 200},{"cardInvoiceNum":
        |15,"tipAmount": 100}]}"""))
        susDelDriverStatusReader.send(susDelDriverEventHandler, "abcd")
    expectNoMessage()
  }
}