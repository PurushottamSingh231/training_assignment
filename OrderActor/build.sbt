name := "OrderActor"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.25"

libraryDependencies += "com.typesafe" % "config" % "1.2.1"

libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.3.0"

libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.26" % Test

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1.1"

//libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.7"

