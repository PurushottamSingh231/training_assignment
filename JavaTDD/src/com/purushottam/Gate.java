package com.purushottam;

public class Gate {
    private String gateId;
    private boolean isOccupied;

    public Gate(String gateId) {
        this.gateId = gateId;
        this.isOccupied = false;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    @Override
    public String toString(){
        return "\nGate with [ gateID= "+this.gateId+ " \t\tAvailable= "+ this.isOccupied +"]"
                +"\n-----------------------------------------------------------------------------";
    }

}
