package com.purushottam;
enum parkingType {
    DOMESTIC,
    PRIVATE;
}

public class ParkingLot {

    private String parkingId;
    private parkingType Ptype;
    private boolean avail;

    public ParkingLot(String parkingId, parkingType ptype) {
        this.parkingId = parkingId;
        this.Ptype = ptype;
        this.avail = false;
    }

    public parkingType getPtype() {
        return Ptype;
    }

    public void setPtype(parkingType ptype) {
        Ptype = ptype;
    }

    public boolean isAvail() {
        return avail;
    }

    public void setAvail(boolean avail) {
        this.avail = avail;
    }

    @Override
    public String toString() {
        return "\nParkingSpot with [" +
                "ParkingId = " + parkingId  + ",\t Type = " + Ptype + "\t Availability = " + avail + "]"+
                "\n----------------------------------------------------------------------------------------";
    }

}
