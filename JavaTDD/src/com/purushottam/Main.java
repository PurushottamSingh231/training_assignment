package com.purushottam;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        List<ParkingLot> parking;
	    AirportUtil airportUtil = new AirportUtil("KTA");
	    airportUtil.add_Terminal(new Terminal("T1",4,3,3));
	    airportUtil.add_Terminal(new Terminal("T2",8,6,5));
	    airportUtil.add_Terminal(new Terminal("T3",6,4,6));
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your Flight- ID");
        String flightId = sc.nextLine();
        System.out.println("Enter Flight Type- {Private,Domestic or International}");
        String flighttype = sc.nextLine();


	    Flight f1 = new Flight(flightId,flighttype);
        airportUtil.getTerminalDetails();
	    f1.setAirportUtil(airportUtil);

	    f1.wantToland();

	    f1.takeoff();

    }
}
