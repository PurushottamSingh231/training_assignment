package com.purushottam;

import java.util.ArrayList;
import java.util.List;

public class AirportUtil {

    private String airportName;
    private List<Terminal> terminals ;
    private Terminal assign;

    public AirportUtil(String airportName) {
        this.airportName = airportName;
        this.terminals = new ArrayList<>();
    }

    public String getAirportName() {

        return airportName;
    }

    public void add_Terminal(Terminal terminal){

        terminals.add(terminal);
    }

    public void getTerminalDetails(){
        for (Terminal t: terminals){
            System.out.println("\n"+ t.toString());
        }
    }

    public List<ParkingLot> getParking(String flightType)
    {

        for (Terminal terminal: terminals)
        {
            List<Gate> gate = terminal.reserveGate();
//            System.out.println(gate.size());
            if(gate != null) {

                List<ParkingLot> parking = terminal.getParkingLot(flightType);
                if (parking != null) {
                    this.assign = terminal;
                    System.out.println("****************************************************************************************************************");
                    System.out.println("Following Gate and Parking slot for your \"" + flightType + "\" Flight are allotted to you at " +
                            "terminal with Terminal ID -  "+ assign.getTerminalId());
                    System.out.println("****************************************************************************************************************");
                    for (Gate g: gate){
                        System.out.println("Gate\n" + g);
                    }
                    for (ParkingLot p : parking) {
                        System.out.print(p + " ");
                    }
                    System.out.println();
                    return parking;
                }
            }
            else {
                System.out.println("Sorry There is no available Gate Please wait we will let you know once Gate will be available");
                System.out.println(gate.size());
                return null;
            }
        }

        System.out.println("Sorry Airport has currently no available parking slot Please wait until it will be available for you");
        return null;


    }

    public void leaveParking(List<ParkingLot> parking){
        if(parking != null){
            this.assign.releaseParking(parking);
        }else{
            System.out.println("Your Flight has not been parked yet You can't Take off");
        }
    }




}
