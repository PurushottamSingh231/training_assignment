package com.purushottam;

import java.util.ArrayList;
import java.util.List;

public class Flight {
    private String flightId;
    private String flightType;
    private List<ParkingLot> parkingLot;
    private AirportUtil airportUtil;

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getFlightType() {
        return flightType;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public Flight(String flightId, String flightType) {
        this.flightId = flightId;
        this.flightType = flightType;
    }




    public AirportUtil getAirport(){
        return airportUtil;
    }

    public void setAirportUtil(AirportUtil airportUtil){
        this.airportUtil = airportUtil;
    }

    public void wantToland()
    {
        AirportUtil airport = getAirport();
        System.out.println("Flight : "+flightId + " Airport : "+airport.getAirportName());
        parkingLot = airport.getParking(flightType);


        if (parkingLot!=null )
        {
            System.out.println("****************************************************************************");
            System.out.println("Your Flight : "+flightId + " has Parked at Airport : "+airport.getAirportName()+" at Above parking slot");
            System.out.println("*****************************************************************************");

        }
    }

    public void takeoff(){

        System.out.println("\n****************************************************************************");
        System.out.println("Your flight is going to take off from "+ this.airportUtil.getAirportName());
        System.out.println("****************************************************************************");

        airportUtil.leaveParking(this.parkingLot);
        System.out.println("\n****************************************************************************");
        System.out.println("After Take OFF Parking slot Status");
        System.out.println("****************************************************************************");
        for(ParkingLot p : this.parkingLot){
            System.out.println(p+" ");
        }

    }

}
