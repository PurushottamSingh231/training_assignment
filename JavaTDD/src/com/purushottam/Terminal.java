package com.purushottam;

import java.util.ArrayList;
import java.util.List;

public class Terminal {

    private String terminalId;
    private int privateSpot;
    private int domesticSpot;
    private  int numberOfGate;
    private List<ParkingLot> privateParking;
    private List<ParkingLot> domesticParking;
    private List<Gate> gates;

    public String getTerminalId() {
        return terminalId;
    }

    public Terminal(String terminalId, int privateSpot, int domesticSpot, int numberOfGate) {
        this.terminalId = terminalId;
        this.privateSpot = privateSpot;
        this.domesticSpot = domesticSpot;
        this.numberOfGate = numberOfGate;

        privateParking = new ArrayList<>();
        domesticParking = new ArrayList<>();
        gates = new ArrayList<>();

        for(int i = 0; i< this.domesticSpot ; i++){
            ParkingLot p = new ParkingLot("Domestic"+i, parkingType.DOMESTIC);
            this.domesticParking.add(p);
        }

        for(int i = 0; i< this.privateSpot ; i++){
            ParkingLot p = new ParkingLot("Private"+i, parkingType.PRIVATE);
            this.privateParking.add(p);
        }

        for(int i = 0;i<this.numberOfGate;i++){
            this.gates.add(new Gate("Gate"+i));
        }

    }

    public List<Gate> reserveGate(){
        List<Gate> availableGate = new ArrayList<>();
        for(Gate g : this.gates){
            if(!g.isOccupied()){
                g.setOccupied(true);
                availableGate.add(g);
                return availableGate;
            }
        }
        return null;
    }

    public void releaseGate(List<Gate> gates){
        for(Gate g : gates ){
            g.setOccupied(false);
        }

    }


    public List<ParkingLot> getParkingLot(String flightType){
        List<ParkingLot> availableParking = new ArrayList<>();
        int number;
        switch (flightType){

            case "International":
                number = 0;
                for(ParkingLot p : this.privateParking){
                    if(!p.isAvail()) {
                        availableParking.add(p);
                        number++;
                        if (number == 8) {
                            for (ParkingLot dp : availableParking) {
                                dp.setAvail(true);
                            }
                            return availableParking;
                        }
                    }
                }
                number =0;
                if(availableParking.size()>0)
                            availableParking.clear();


                for(ParkingLot p : this.domesticParking){
                    if(!p.isAvail()) {
                        availableParking.add(p);
                        number++;
                        if (number == 5) {
                            for (ParkingLot dp : availableParking) {
                                dp.setAvail(true);
                            }
                            return availableParking;
                        }
                    }
               }
                if(availableParking.size()>0)
                    availableParking.clear();

                break;


            case "Domestic":

                for(ParkingLot p : this.domesticParking){
                    if(!p.isAvail()){
                        p.setAvail(true);
                        availableParking.add(p);
                        return availableParking;
                    }
                }
                number = 0;

                for(ParkingLot p : this.privateParking){
                    if(!p.isAvail()) {
                        availableParking.add(p);
                        number++;
                        if (number == 2) {
                            for (ParkingLot dp : availableParking) {
                                dp.setAvail(true);
                            }
                            return availableParking;
                        }
                    }
                }
                if(availableParking.size()>0)
                    availableParking.clear();

                break;

            case "Private":
                for(ParkingLot p : this.privateParking){
                    if(!p.isAvail()){
                        p.setAvail(true);
                        availableParking.add(p);
                        return availableParking;
                    }
                }

                for(ParkingLot p : this.domesticParking){
                    if(!p.isAvail()){
                        p.setAvail(true);
                        availableParking.add(p);
                        return availableParking;
                    }
                }

                break;

        }
        return null;
    }


    public void releaseParking(List<ParkingLot> parkingLot){
        for (ParkingLot p : parkingLot){
            p.setAvail(false);
        }
    }



    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("AIRPORT [ \nTerminalId = "+this.terminalId+" \n");

        stringBuilder.append("\nGates : \n");
        for (Gate g : this.gates)
        {
            stringBuilder.append(g.toString());
        }

        stringBuilder.append("\n\nPrivate Parking Spot : \n");
        for (ParkingLot parkingLot : this.privateParking)
        {
            stringBuilder.append(parkingLot.toString());
        }


        stringBuilder.append("\n\nDomestic Parking Spot : \n");
        for (ParkingLot parkingLot : this.domesticParking)
        {
            stringBuilder.append(parkingLot.toString());
        }


        stringBuilder.append("\n ]\n**************************************************************************" +
                                    "**************\n");


        return stringBuilder.toString();
    }


}
