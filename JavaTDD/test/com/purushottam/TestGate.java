package com.purushottam;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestGate {
    @Test
    public void testAvailableGate(){
//        fail();
        Gate g = new Gate("Gate01");

        assertFalse(g.isOccupied());

        g.setOccupied(true);

//        assertFalse(g.isOccupied());
        assertTrue(g.isOccupied());

    }
}
