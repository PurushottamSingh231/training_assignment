package com.purushottam;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestFlight {
    @Test
    public void TestgetAirport(){
        AirportUtil airportUtil = new AirportUtil("ccu");
        Flight f = new Flight("DB-327","Domestic");
        f.setAirportUtil(airportUtil);
//        assertNotEquals("ccu",f.getAirport().getAirportName());
        assertEquals("ccu",f.getAirport().getAirportName());
        assertEquals("Domestic",f.getFlightType());
    }
}
