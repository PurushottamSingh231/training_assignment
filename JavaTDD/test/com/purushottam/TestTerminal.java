package com.purushottam;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestTerminal {
    @Test
    public void testgetParking(){

        /* Case1 :-   private parking has 8 spot and domestic parking has 10 spot so
                any flight can land up and take the space
                should be all test pass
         */

        Terminal t1 = new Terminal("T1",8,10,4);
        assertNotNull(t1.getParkingLot("International"));
        assertNotNull(t1.getParkingLot("Private"));
        assertNotNull(t1.getParkingLot("Domestic"));

        /* case2 :- When private parking has less than 8 spot and domestic parking
            has greater than equal to 5 spot
            should be all test pass
         */

        Terminal t2 = new Terminal("T2",4,6,3);

        assertNotNull(t2.getParkingLot("International"));
        assertNotNull(t2.getParkingLot("Private"));
        assertNotNull(t2.getParkingLot("Domestic"));

        /* case3 :- When private parking has greater than 8 spot and
                domestic parking has less than 5 spot

                should be all test pass
         */

        Terminal t3 = new Terminal("T3",9,3,2);

        assertNotNull(t3.getParkingLot("International"));
        assertNotNull(t3.getParkingLot("Private"));
        assertNotNull(t3.getParkingLot("Domestic"));

        /* case4 :- When private parking has less than 8 spot and domestic parking
            has less than 5 spot
            test will pass for private and domestic flight but not for International fligt
         */

        Terminal t4 = new Terminal("T4",5,4,5);

//        assertNotNull(t4.getParkingLot("International"));           //will fail()
        assertNotNull(t4.getParkingLot("Private"));             //pass()
        assertNotNull(t4.getParkingLot("Domestic"));            //pass()


    }
}
