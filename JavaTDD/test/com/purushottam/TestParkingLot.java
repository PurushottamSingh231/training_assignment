package com.purushottam;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestParkingLot {

    @Test
    public void parkingAvailabilityTest(){
//        fail();

        ParkingLot parkingLot = new ParkingLot("1",parkingType.DOMESTIC);
        assertFalse(parkingLot.isAvail());

        parkingLot.setAvail(true);
//        assertFalse(parkingLot.isAvail());
        assertTrue(parkingLot.isAvail());

    }

    @Test
    public void parkingTypeTest(){
//        fail();
        ParkingLot parkingLot = new ParkingLot("1",parkingType.PRIVATE);
        assertEquals(parkingType.PRIVATE,parkingLot.getPtype());

        parkingLot.setPtype(parkingType.DOMESTIC);
//        assertEquals(parkingType.PRIVATE,parkingLot.getPtype());
        assertEquals(parkingType.DOMESTIC,parkingLot.getPtype());


    }


}
